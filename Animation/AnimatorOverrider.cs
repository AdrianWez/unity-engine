/*
    Basic animator overrider manager.
*/
using UnityEngine;
namespace AdrianWez
{
    namespace Animation
    {

        public class AnimatorOverrider : MonoBehaviour
        {
            // animator itself
            private Animator animator { get => GetComponent<Animator>(); }

            // caching the first/base RAC
            private RuntimeAnimatorController zero;
            private void Awake()
            {
                zero = animator.runtimeAnimatorController;
            }

            public void SetOverrider(AnimatorOverrideController overrider = null)
            {
                // reset to the zero(base) RAC if no overrider is passed.
                if (overrider != null)
                    animator.runtimeAnimatorController = overrider;
                else animator.runtimeAnimatorController = zero;
            }
        }
    }
}
