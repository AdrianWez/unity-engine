using UnityEngine;
namespace AdrianWez
{
    namespace Controllers
    {
        [RequireComponent(typeof(Rigidbody))]
        public class Hover : MonoBehaviour
        {
            [Header("Vehicle")]
            [SerializeField] private Transform[] _springs;                      // spring from which force will be applyed
            [SerializeField] private Transform _centerOfMass;                   // the center of mass of the hover
            [SerializeField] private Transform _propulsor;                      // where power will be applyed to the 'engine' of the hover
            [SerializeField] private float _groundDistance = 3;                 // distance from the ground till the hover stabilized postion
            [Header("Engine")]
            [SerializeField] private float _power = 500;                        // Acceleration / Deceleration force
            [SerializeField] private float _torque = 300;                       // Rotation force
            private Rigidbody _rigidbody { get => GetComponent<Rigidbody>(); }  // hover's rigidbody
            RaycastHit _hit;                                                    // cached springs hits


            // inputs
            private Vector2 _moveInput { get => new(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")); }
            [Header("Breaks")]
            [SerializeField] private KeyCode _break = KeyCode.Space;
            // Using fixed update to apply force in the physic engine.
            private void FixedUpdate()
            {
                if(Input.GetKey(_break))
                {
                    // Retarding velocities applyed on the rigidbody
                    _rigidbody.velocity = _rigidbody.velocity * 0.9f;
                    _rigidbody.angularVelocity = _rigidbody.angularVelocity * 0.9f;
                }
                else
                {
                    // Acceleration / Deceleration
                    _rigidbody.AddForceAtPosition(Time.deltaTime * transform.TransformDirection(Vector3.forward) * _moveInput.y * _power, _propulsor.position);
                    // Torque
                    _rigidbody.AddTorque(Time.deltaTime * transform.TransformDirection(Vector3.up) * _moveInput.x * _torque);
                }
                // Applying for to each springs in the hover
                foreach (Transform _spring in _springs)
                    if(Physics.Raycast(_spring.position, transform.TransformDirection(Vector3.down), out _hit, _groundDistance))
                        _rigidbody.AddForceAtPosition(Time.deltaTime * transform.TransformDirection(Vector3.up) * Mathf.Pow(3 - _hit.distance, 2)/3 * 250, _spring.position);
                // depenetrating side collisions
                _rigidbody.AddForce(-Time.deltaTime * transform.TransformVector(Vector3.right) * transform.InverseTransformVector(_rigidbody.velocity).x * 5);
            }
        }
    }
}