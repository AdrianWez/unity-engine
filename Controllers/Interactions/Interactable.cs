/*
    Component to be placed at a Interactable object (ex.: NPCs, game items and etc.)
*/
using UnityEngine;
public enum InteractionType
{ Click, Hold } // Different types to trigger the interaction
public abstract class Interactable: MonoBehaviour {

    private float _holdTime;
    [Tooltip("Required time in seconds to trigger the interaction.")]
    [SerializeField] private float _requiredHoldTime = 3;
    [Tooltip("The specific type of interaction required by this object.")]
    public InteractionType _interactionType;
    // Function to describe what happens when interaction triggers.
    public abstract string GetDescription();
    // The interaction itself.
    public abstract void Interact();
    // Function to increase time count if is a Hold interaction.
    public void IncreaseHoldTime() => _holdTime += Time.deltaTime / _requiredHoldTime;
    public void ResetHoldTime() => _holdTime = .0f;
    // Current hold time counter.
    public float GetHoldTime() => _holdTime;
}
