using UnityEngine;
namespace AdrianWez
{
    namespace Controllers
    {

    public class Interactor : MonoBehaviour
    {
        [Header("Interactions")]
        [SerializeField] private float _interactionDistance = 1.5f;             // distance between player and interatable object to trigger the process
        [SerializeField] private Transform _origin;                             // origin of the ray to check for interactable components
        [SerializeField] private TMPro.TMP_Text _interactionText;               // UI to display returned discription from interactable object
        [SerializeField] private UnityEngine.UI.Image _interactionHoldProgress; // UI progress bar to display passage of time while holding the interaction key
        private void Update()
        {
            if (Physics.Raycast(_origin.position, transform.forward, out RaycastHit _hit, _interactionDistance) && _hit.collider.GetComponent<Interactable>())
            {
                Interactable _interactable = _hit.collider.GetComponent<Interactable>();

                _interactionText.text = _interactable.GetDescription();
                _interactionHoldProgress.transform.parent.gameObject.SetActive(_interactable._interactionType == Interactable.InteractionType.Hold);

                // interacting accordingly with the interactable found
                switch (_interactable._interactionType)
                {
                    case Interactable.InteractionType.Click:
                        if (Input.GetKeyDown(KeyCode.LeftShift)) _interactable.Interact();
                        break;
                    case Interactable.InteractionType.Hold:
                        if (Input.GetKey(KeyCode.LeftShift))
                        {
                            _interactable.IncreaseHoldTime();
                            if (_interactable.GetHoldTime() > 1f)
                            {
                                _interactable.Interact();
                                _interactable.ResetHoldTime();
                            }
                        }
                        else _interactable.ResetHoldTime();
                        _interactionHoldProgress.fillAmount = _interactable.GetHoldTime();
                        break;
                }
            }
            else
            {
                // no interactable object in range
                _interactionText.text = "";
                _interactionHoldProgress.transform.parent.gameObject.SetActive(false);
            }
        }
    }
    }
}
