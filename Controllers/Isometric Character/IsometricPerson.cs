using UnityEngine;
namespace AdrianWez
{
    namespace Controllers
    {
        // Basic Isometric Character Controller
        [RequireComponent(typeof(CharacterController))]
        [RequireComponent(typeof(Animator))]
        public class IsometricPerson : MonoBehaviour
        {
            [Header("Player")]
            [SerializeField] private float _walkSpeed = 1;
            [Tooltip("Move speed of the character in m/s")]
            [SerializeField] private float _speedVariation = 4;
            [SerializeField] private float _runSpeed = 3;

            [Tooltip("Sprint speed of the character in m/s")]
            [SerializeField] private float _sprintSpeed = 6;

            [Tooltip("How fast the character turns to face movement direction")]
            [Range(0.0f, 0.3f)]
            [SerializeField] private float RotationSmoothTime = 0.12f;

            [Tooltip("The character uses its own gravity value. The engine default is -9.81f")]
            [SerializeField] private float Gravity = -15.0f;

            [Tooltip("Time required to pass before entering the fall state. Useful for walking down stairs")]
            [SerializeField] private float FallTimeout = 0.15f;


            [Tooltip("Useful for rough ground")]
            [SerializeField] private float GroundedOffset = -0.14f;

            [Tooltip("The radius of the grounded check. Should match the radius of the CharacterController")]
            [SerializeField] private float GroundedRadius = 0.28f;

            [Tooltip("What layers the character uses as ground")]
            public LayerMask GroundLayers;
            
            // player
            private float _speed;
            private float _targetSpeed;
            private Vector3 _targetDirection;
            private float _rotation;
            private float _targetRotation = 0.0f;
            private float _rotationVelocity;
            private float _verticalVelocity;
            private float _terminalVelocity = 53.0f;
            private Vector3 _spherePosition;            // Sphere used to check if player is grounded

            // timeout deltatime
            private float _fallTimeoutDelta;

            // animation IDs
            private int _animIDSpeed;
            private int _animIDGrounded;
            private int _animIDFreeFall;

            // hidden refs
            private Animator _animator { get => GetComponent<Animator>(); }
            private CharacterController _controller { get => GetComponent<CharacterController>(); }

            private Vector2 _moveInput { get => new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")); }
            private Vector3 _normalizedMove;

            // states
            public static bool Grounded;      // Used to check if player is touching the ground
            public static bool Moving;        // is player moving horizontally
            public static bool Sprinting;      // Move speed acceleration 

            private void Start()
            {

                AssignAnimationIDs();

                // reset our timeouts on start
                _fallTimeoutDelta = FallTimeout;
            }

            private void Update()
            {

                States();
                FallAndGravity();
                Move();
            }

            private void AssignAnimationIDs()
            {
                _animIDSpeed = Animator.StringToHash("Speed");
                _animIDGrounded = Animator.StringToHash("Grounded");
                _animIDFreeFall = Animator.StringToHash("FreeFall");
            }

            private void States()
            {
                // set sphere position, with offset
                _spherePosition = new Vector3(transform.position.x, transform.position.y - GroundedOffset, transform.position.z);
                Grounded = Physics.CheckSphere(_spherePosition, GroundedRadius, GroundLayers, QueryTriggerInteraction.Ignore);

                // update animator
                _animator.SetBool(_animIDGrounded, Grounded);

                Sprinting = Grounded && Input.GetKey(KeyCode.LeftShift) && _moveInput != Vector2.zero;
                Moving = new Vector3(_controller.velocity.x, 0, _controller.velocity.z).magnitude != .0f;
            }

            private void Move()
            {
                // set target speed based on move speed, sprint speed and if sprint is pressed

                // if there is no input, set the target speed to 0
                if (_moveInput == Vector2.zero) _targetSpeed -= Time.deltaTime * _speedVariation;
                else _targetSpeed += Time.deltaTime * _speedVariation;
                _targetSpeed = Mathf.Clamp(_targetSpeed, _walkSpeed, _runSpeed);
                _speed = Sprinting ? _sprintSpeed : Vector2.ClampMagnitude(_moveInput, 1).magnitude * _targetSpeed;

                // normalise input direction
                _normalizedMove = new Vector3(_moveInput.x, 0.0f, _moveInput.y).normalized;

                // note: Vector2's != operator uses approximation so is not floating point error prone, and is cheaper than magnitude
                // if there is a move input rotate player when the player is moving
                if (_moveInput != Vector2.zero)
                {
                    _targetRotation = Mathf.Atan2(_normalizedMove.x, _normalizedMove.z) * Mathf.Rad2Deg + -45;
                    _rotation = Mathf.SmoothDampAngle(transform.eulerAngles.y, _targetRotation, ref _rotationVelocity, RotationSmoothTime);
                    // rotate to face input direction relative to camera position
                    transform.rotation = Quaternion.Euler(0.0f, _rotation, 0.0f);
                }

                _targetDirection = Quaternion.Euler(0.0f, _targetRotation, 0.0f) * Vector3.forward;

                // move the player
                _controller.Move(_targetDirection.normalized * (_speed * Time.deltaTime) + new Vector3(0.0f, _verticalVelocity, 0.0f) * Time.deltaTime);

                // updating animator
                _animator.SetFloat(_animIDSpeed, _speed, .1f, Time.deltaTime);
            }

            private void FallAndGravity()
            {
                if (Grounded)
                {
                    // reset the fall timeout timer
                    _fallTimeoutDelta = FallTimeout;

                    // update animator if using character
                    _animator.SetBool(_animIDFreeFall, false);

                    // stop our velocity dropping infinitely when grounded
                    if (_verticalVelocity < 0.0f) _verticalVelocity = -2f;

                }
                else
                {
                    // fall timeout
                    if (_fallTimeoutDelta >= 0.0f) _fallTimeoutDelta -= Time.deltaTime;
                    // update animator
                    else _animator.SetBool(_animIDFreeFall, true);
                }

                // apply gravity over time if under terminal (multiply by delta time twice to linearly speed up over time)
                if (_verticalVelocity < _terminalVelocity)
                    _verticalVelocity += Gravity * Time.deltaTime;
            }

            // Gizmos
            private void OnDrawGizmosSelected()
            {
                Color transparentGreen = new Color(0.0f, 1.0f, 0.0f, 0.35f);
                Color transparentRed = new Color(1.0f, 0.0f, 0.0f, 0.35f);

                if (Grounded) Gizmos.color = transparentGreen;
                else Gizmos.color = transparentRed;

                // when selected, draw a gizmo in the position of, and matching radius of, the grounded collider
                Gizmos.DrawSphere(new Vector3(transform.position.x, transform.position.y - GroundedOffset, transform.position.z), GroundedRadius);
            }
        }
    }
}
