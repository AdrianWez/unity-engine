using UnityEngine;
namespace AdrianWez
{
    namespace Design
    {
        public class MultiScenes : MonoBehaviour
        {

            // Assuming the object carrying this script is named after the scene it should load...
            private string sceneToload { get => name; }
            // Either by trigger collision or distance. 
            public enum LoadType
            { Trigger, Distance }
            /* TODO...
                Implement load scene by distance
            */
            [SerializeField] private LoadType _type;

            private void OnTriggerEnter(Collider collider)
            {
                if (collider.CompareTag("Player"))
                    ChangeScene();
            }
            private void OnTriggerExit(Collider collider)
            {
                if (collider.CompareTag("Player"))
                    UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync(sceneToload);
            }

            private void ChangeScene()
            {
                // if scene is already loaded, do nothing
                if (UnityEngine.SceneManagement.SceneManager.GetSceneByName(sceneToload).isLoaded) return;
                // load scene
                UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(sceneToload, UnityEngine.SceneManagement.LoadSceneMode.Additive);
                // TODO
                // update emvironment

            }
        }
    }
}
