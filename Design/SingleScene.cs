using UnityEngine;
// Easy and quick transition to a scene that should be loaded in single mode.
namespace AdrianWez
{
    namespace Design
    {

        public class SingleScene : MonoBehaviour
        {
            // Assuming the object carrying this script is named after the scene it should load...
            private string sceneToload { get => name; }
            [Tooltip("Position where player should be spawned after loading the scene.")]
            [SerializeField] private Transform spawnPoint;

            private void OnTriggerEnter(Collider collider)
            {
                if (collider.CompareTag("Player"))
                    StartCoroutine(Transition(collider.GetComponent<Controllers.ThirdPerson>()));
            }

            private System.Collections.IEnumerator Transition(Controllers.ThirdPerson player)
            {
                yield return null;
                // lock player's inputs
                player.LockedInputs = true;

                //Begin to load the Scene you specify
                AsyncOperation async = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(sceneToload, UnityEngine.SceneManagement.LoadSceneMode.Single);
                //Don't let the Scene activate until you allow it to
                async.allowSceneActivation = false;
                //When the load is still in progress, output the Text and progress bar
                while (!async.isDone)
                {
                    // Check if the load has finished
                    if (async.progress >= 0.9f)
                    {
                        // move player
                        player.transform.SetPositionAndRotation(spawnPoint.position, spawnPoint.rotation);
                        
                        // Enable player controls
                        player.LockedInputs = false;
                    }

                    yield return null;
                }
            }
        }
    }
}
